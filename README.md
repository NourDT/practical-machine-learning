# README #

### What is this repository for? ###

* This repository is for the CourseProject of Practical MAchine Learning Class
* Version 1.0
* Link to the documentation is [here](https://bitbucket.org/bsudyne/practical-machine-learning/downloads/markdown.html)

### How do I get set up? ###

* The code is in the Code/ folder
* Files used during the work and produced plots are in the Resources/ folder
* to run classifier.R, change "filePath" variable to reflect your filepath to the .csv files