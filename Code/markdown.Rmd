---
title: "Practical Machine Learning"
author: "Barbara Sudy"
date: "20/08/2014"
output:
  html_document:
    number_sections: yes
---
# Preprocessing the Data
* The training data was split into training ant test sets - 70% vs 30% 
* The data was cleaned from any variables that mostly consists of "NA" or blank fields 
* All numeric variables were standardized in the training set using the <i>preProcess()</i> finction in caret
* The test set was standardized using the parameters obtained by standardizing the training set

```{r preproc, eval=FALSE}
library(caret)
filePath_train <- '/home/barbi/Workspaces/PracticalML/Resources/pml-training.csv'
training <- read.csv(filePath_train)

### getting numeric subdataframe and remove columns with NA's
nums <- sapply(training, is.numeric) ; training_num <- training[ , nums]  
training_num <- training_num[ , apply(training_num, 2, function(x) !any(is.na(x)))]
training_num$classe <- training$classe

### splitting data
set.seed(123)
samp <- sample(2, nrow(training), replace = TRUE, prob = c(0.7,0.3))
trainSet_num <- training_num[samp == 1,]
testSet_num  <- training_num[samp == 2,]

### Standardization
preObj <- preProcess(trainSet_num[,1:56], method=c("center","scale"))
trainSet_st <- predict(preObj, trainSet_num[,1:56])
trainSet_st$classe <- trainSet_num$classe
testSet_st <- predict(preObj, testSet_num[,1:56])
testSet_st$classe <- testSet_num$classe
```


# Models Used for Classification
## Decision tree - rpart
* First model - decision tree using <i>rpart</i> with <i>caret</i>
* Predictors were obtained from a PCA on the numeric variables that captures 80% of the variance

```{r rpart, eval = FALSE}
library(rpart)
## PCA
pca <- preProcess(trainSet_st[,1:56], method = "pca", thresh = 0.8)
trainPC <- predict(pca, trainSet_st[,1:56])

## Training Process
set.seed(123)
modelFit <- train(trainSet_st$classe ~., method = "rpart", data = trainPC)
modelFit$finalModel
trainPred <- predict(modelFit, trainPC) ; confusionMatrix(trainPred, trainSet_num$classe)  ## In sample error: 54.3%

## Prediction on the test set
testPC <- predict(pca, testSet_st[,1:56])
prediction <- predict(modelFit, testPC) ; print(length(prediction))
confusionMatrix(testSet_num$classe, prediction)  ## Out of sample error: 54.3%

```

* Confusion Matrix and Statistics on the test set

                    Reference
          Prediction    A    B    C    D    E
                    A 1099  215  183    0  167
                    B  489  384  184    0   43
                    C  328   60  605    0   50
                    D  153  101  495    0  188          Accuracy : 0.4572
                    E  132  121  247    0  570          95% CI : (0.4443, 0.4701)
                                                        
* High In and Out of sample errors, low Accuracy
* The model does not classify any records to the D category
 
## Support Vector Machine with Radial Basis Kernel Function

* Predictors are obtained from a PCA on the numeric variables with 5 components
* <i>trainControl()</i> function set to 5-fold cross validation

```{r , eval=FALSE}
## PCA with 5 components
pca_svm <- preProcess(trainSet_st[,1:56], method = "pca", pcaComp = 5)
trainPC_svm <- predict(pca_svm, trainSet_st[,1:56])
## Setting up trainControl() function
tc = trainControl(method = "cv", number = 5)

## Training process
set.seed(825)
svmFit <- train(trainSet_st$classe ~ ., data = trainPC_svm, 
                method = "svmRadial", trControl = tc)
svmFit$finalModel

## Predicting on test data
testPC_svm <- predict(pca_svm, testSet_st[,1:56])
prediction_svm <- predict(svmFit, testPC_scv)

## Correct predictions
predRight_svm <- prediction_svm == testSet_st$classe
## Confusion Matrix on the test set
confusionMatrix(prediction_svm, testSet_st$classe)

```                  

* Confusion Matrix and Statistics on the test set

                    Reference
          Prediction    A    B    C     D    E          Gaussian Radial Basis kernel function.
                    A 1223  189  288   95   82          Hyperparameter : sigma =  0.302566999788479                     
                    B   82  577  106   69   98          Number of Support Vectors : 10927
                    C  135   93  428  108   71          OOB estimate of  error rate: 39.6%
                    D  189  129  180  609  141          Accuracy : 0.6046          
                    E   35  112   41   56  678          95% CI : (0.5919, 0.6172)
                                                        
                 
* By classes

                             Class: A Class: B Class: C Class: D Class: E
        Sensitivity            0.7350  0.52455  0.41035   0.6499   0.6336
        Specificity            0.8424  0.92469  0.91469   0.8690   0.9486
        Pos Pred Value         0.6516  0.61910  0.51257   0.4880   0.7354
        Neg Pred Value         0.8880  0.89287  0.87648   0.9282   0.9199

* Plot of correct vs incorrect predictions
<body> 
<table border="0" style="margin: auto; width: 900px;">
<tr>
    <td><img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/SVM.png">
    </td>
    <td> <img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/SVM3.png">
    </td>
</tr>
</table>
</body>

## Random Forests
### Preorocessing with PCA, 3 components - 5-fold cross validation

* Predictors were obtained from a PCA on the numeric variables with 3 components
* Confusion Matrix and Statistics on the test set (No code chunk for this model)

                    Reference
          Prediction    A    B    C     D    E          Type of random forest: classification
                    A 1241  145  147   82   86          Number of trees: 500
                    B   95  651  118   83  120          No. of variables tried at each split: 2
                    C  132  124  642  105  103          OOB estimate of  error rate: 34.47%
                    D  122   95   93  583  100          Accuracy : 0.6498      
                    E   74   85   43   84  661          95% CI : (0.6374, 0.6621)
                 
* By classes

                             Class: A Class: B Class: C Class: D Class: E
        Sensitivity            0.7458   0.5918   0.6155   0.6222   0.6178
        Specificity            0.8892   0.9118   0.9027   0.9159   0.9397
        Pos Pred Value         0.7296   0.6101   0.5805   0.5871   0.6980
        Neg Pred Value         0.8972   0.9054   0.9148   0.9266   0.9160
        
        
* Plot of correct vs incorrect predictions
<body> 
<table border="0" style="margin: auto; width: 900px;">
<tr>
    <td><img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandForest PCA 3.png">
    </td>
    <td> <img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandForest PCA 3 3.png">
    </td>
</tr>
</table>
</body>

### Preorocessing with PCA, 5 components - 5-fold cross validation

* Predictors were obtained from a PCA on the numeric variables with 5 components

```{r Random Forest, eval=FALSE}
###### Random Forest with 5 components
library(randomForest)
## PCA
pca_rf2 <- preProcess(trainSet_st[,1:56], method = "pca", pcaComp = 5)
trainPC_rf2 <- predict(pca_rf2, trainSet_st[,1:56])
## Setting up trainControl() function
tc = trainControl(method = "cv", number = 5) # 5-fold cross validation

##Training process
set.seed(17)
modelFit_rf2 <- train(trainSet_st$classe ~ ., method = "rf",
                      trainControl = tc, data = trainPC_rf2)
modelFit_rf2$finalModel

## Prediction on the test set
testPC_rf2 <- predict(pca_rf2, testSet_st[,1:56])
prediction_rf2 <- predict(modelFit_rf2, testPC_rf2)
## Correct classifications
predRight_rf2 <- prediction_rf2 == testSet_st$classe
## ConfusionMatrix on the test set
confusionMatrix(prediction_rf2, testSet_st$classe)

qplot(PC1, PC2, data = testPC_rf2, colour = c(testSet_st$classe,predRight_rf2), main = "Prediction on test set - Random Forest \n 5 PCA component" )

```

* Confusion Matrix and Statistics on the Training and Test Set

                Type of random forest: classification
                Number of trees: 500
                No. of variables tried at each split: 2
                OOB estimate of  error rate: 14.47%


              Training      Reference                                       Test       Reference
          Prediction    A    B    C    D    E   class.error         Prediction    A    B    C    D    E
                    A 3541  100  131   98   46  0.09576098                    A 1493   59   42   43   29
                    B  179 2163  153   96  106  0.19799778                    B   38  917   56   14   45
                    C  114  139 1955  120   51  0.17822615                    C   66   61  880   86   41
                    D   86   51  148 1937   57  0.15006582                    D   43   40   47  773   28
                    E   70  110   95   96 2166  0.14623571                    E   24   23   18   21  927
                    
                                                                          Accuracy : 0.8583         
                                                                          95% CI : (0.849, 0.8671)
  
                  
* By classes

                             Class: A Class: B Class: C Class: D Class: E
        Sensitivity            0.8972   0.8336   0.8437   0.8250   0.8664
        Specificity            0.9583   0.9675   0.9468   0.9676   0.9819
        Pos Pred Value         0.8962   0.8570   0.7760   0.8303   0.9151
        Neg Pred Value         0.9588   0.9614   0.9652   0.9664   0.9702

* Plot of correct vs incorrect predictions
<body> 
<table border="0" style="margin: auto; width: 900px;">
<tr>
    <td><img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandomForest PCA 5.png">
    </td>
    <td> <img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandomForest PCA 5 3.png">
    </td>
</tr>
</table>
</body>

* Original Exercise Groups vs Predicted Exercise Groups

<body> 
<table style="margin: auto; width: 900px;">
<tr>
    <td> 
        <div style="text-align: center;">
            <b>Original      </b>
        </div>
    </td>
    <td> <div style="text-align: center;">
            <b>Prediction    </b>
        </div>
    </td>
</tr>
<tr>
    <td><img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/Exercise 1.png">
    </td>
    <td> <img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandForest predictions 1.png">
    </td>
</tr>
<tr>
    <td><img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/Exercise 3.png">
    </td>
    <td> <img src = "/home/barbi/Workspaces/PracticalML/Resources/Plots/RandForest predictions 3.png">
    </td>
</tr>
</table>
</body>
## Summary of Models

<div style="text-align: center;">
```{r, echo=FALSE}
Model_Errors <- c(1,2,3,4)
errors <- c(0.54, 0.39, 0.35, 0.14)
accuracy <- c(0.45, 0.60, 0.65, 0.85)
label <- c("Dec Tree", "SVM", "RForest 3 comp", "RForest 5 comp")

## Error plot
plot(x = Model_Errors, y = errors, ylim=c(0,1),  ylab= "", xlab ="",
     main="Out of sample error \n and Accuracy across models", 
     axes = FALSE, type="b", col="red")
text(Model_Errors,  errors, labels=errors, cex= 0.7, pos=3, col="red")

## Accuracy plot
par(new=T)
plot(x = Model_Errors, y = accuracy, ylim=c(0,1), ylab= "", xlab ="",
     axes = FALSE, type="b", col="green")
text(Model_Errors,  accuracy, labels=accuracy, cex= 0.7, pos=3, col="green")
axis(side = 1, at = Model_Errors, labels = label)
axis(side = 2);

legend(1, 1, c("Error","Accuracy"), col = c("red", "green"), pch = 1,
       text.col = c("red", "green"))
abline(h = seq(0, 1, 0.2), v = Model_Errors, col = "lightgray", lty = 3);
box();

```
</div>

* The lowest out of sample error was obtained by using Random Forests on 5 predictors
* The model correctly classified over 85% of the cases in the test set

# Predicting the Test Cases with Random Forest on 5 predictors

```{r predictions, eval=FALSE}
filePath_test <- '/home/barbi/Workspaces/PracticalML/Resources/pml-testing.csv'
valid <- read.csv(filePath_test)

## Preprocessing the data
valid_num <- valid[,nums]
valid_num <- valid_num[ , apply(valid_num, 2, function(x) !any(is.na(x)))]
valid_st <- predict(preObj, valid_num)
validPC_rf <- predict(pca_rf2, valid_st)

## Prediction
pred_valid_rf <- predict(modelFit_rf2, validPC_rf)
pred_valid_rf

```


      [1] B A B A A E D B A A B C B A E E A B B B
      Levels: A B C D E


